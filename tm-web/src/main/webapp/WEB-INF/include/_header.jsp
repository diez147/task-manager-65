<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Manager</title>
    <style>
        td {
            padding: 5px;
        }
        th {
            color: white;
            font-weight: 700;
            text-align: left;
            background: black;
        }
    </style>
</head>
<body>
    <table width="100%" height="100%" border="1" style="padding: 10px; border-collapse: collapse;">
        <tr>
            <td height="35" width="200" nowrap="nowrap" align="center">
                <b>Task Manager</b>
            </td>

            <td width="100%" align="right">
                <a href="/projects">Проекты</a>
                <a href="/tasks">Задачи</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="100%" valign="top" style="padding: 10px;">