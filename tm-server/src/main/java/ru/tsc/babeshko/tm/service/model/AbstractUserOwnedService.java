package ru.tsc.babeshko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.model.IUserOwnedService;
import ru.tsc.babeshko.tm.comparator.CreatedComparator;
import ru.tsc.babeshko.tm.comparator.DateBeginComparator;
import ru.tsc.babeshko.tm.comparator.StatusComparator;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.exception.system.EmptyStatusException;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;
import ru.tsc.babeshko.tm.repository.model.AbstractUserOwnedRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel> extends AbstractService<M>
        implements IUserOwnedService<M> {

    @NotNull
    @Override
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @NotNull
    protected org.springframework.data.domain.Sort getSortType(@NotNull final Comparator comparator) {
        @NotNull final String fieldName;
        if (comparator == CreatedComparator.INSTANCE) fieldName =  "created";
        else if (comparator == StatusComparator.INSTANCE) fieldName = "status";
        else if (comparator == DateBeginComparator.INSTANCE) fieldName =  "dateBegin";
        else fieldName =  "name";
        return org.springframework.data.domain.Sort.by(org.springframework.data.domain.Sort.Direction.ASC, fieldName);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId, getSortType(comparator));
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final ru.tsc.babeshko.tm.enumerated.Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Nullable
    @Override
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public M changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }

}